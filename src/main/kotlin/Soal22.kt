fun main() {
    print("Masukan Angka : ")
    var arr1 = readln().toString().split(" ").toTypedArray()
    var arr = Array(arr1.size) { 0 }

    for (i in 0 until arr1.size) {
        arr[i] = arr1[i].toInt()
    }

    var deretFibo = deretFibo(arr.size)

    var waktu = Array(arr.size){0}
    var a = 1

    for (i in 0 until arr.size) {
        var b = arr[i]
        do {
            b -= deretFibo[i]
            a++
        } while (b > 0)
        waktu[i] = a
        a = 1
    }

    var min = waktu.min()

    for (i in 0 until waktu.size){
        if (min == waktu[i]){
            println("Lilin ke-${i+1}")
        }
    }
}

fun deretFibo(n: Int): Array<Int> {
    var arrayFibo = Array(n) { 0 }
    arrayFibo[0] = 1
    arrayFibo[1] = 1

    for (i in 2..arrayFibo.size - 1) {
        arrayFibo[i] = arrayFibo[i - 2] + arrayFibo[i - 1]
    }

    return arrayFibo
}