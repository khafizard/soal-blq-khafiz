import kotlin.math.min

fun main() {
    print("Masukan Angka : ")
    var arr1 = readln().toString().split(" ").toTypedArray()
    var arr = Array(arr1.size) { 0 }

    for (i in 0 until arr1.size) {
        arr[i] = arr1[i].toInt()
    }

    arr.sort()

    var minimal = 0
    for (i in 0 until 4) {
        minimal += arr[i]
    }

    var maksimal = 0
    for (i in arr.size-1 downTo arr.size - 4) {
        maksimal += arr[i]
    }

    println(minimal)
    println(maksimal)
}