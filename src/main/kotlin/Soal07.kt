

fun main() {
    print("Masukan Angka : ")
    var arr1 = readln().toString().split(" ").toTypedArray()
    var arr = Array(arr1.size){0}

    for (i in 0 until arr1.size) {
        arr[i] = arr1[i].toInt()
    }

    // Mencari Mean
    var sum = 0
    for (i in 0 until arr.size) {
        sum += arr[i]
    }
    var mean = sum / arr.size
    println("Mean:$mean")

    // Mencari Median
    var tengah = 0
    if (arr.size % 2 == 1) {
        tengah = arr.size / 2
        println("Median:${arr[tengah]}")
    } else {
        tengah = arr.size / 2 - 1
        var median = (arr[tengah] + arr[tengah + 1]) / 2.0
        println("Median:$median")
    }

    // Mencari Modus
    arr.sort()
    var modusJumlah = 0
    var modusSekarang = 0
    var modusSebelum = 0
    var modus = arr[0]

    for (i in 0 until arr.size) {
        for (j in 0 until arr.size) {
            if (arr[i] == arr[j]) {
                modusJumlah += 1
            }
        }
        if (modusJumlah == modusSekarang) {
            if (arr[i] < modus) {
                modus = arr[i]
            }
        } else if (modusJumlah > modusSekarang) {
            modus = arr[i]
        }
        modusSebelum = modusSekarang
        modusSekarang = modusJumlah
        modusJumlah = 0
    }

    println("Modus : $modus")

}