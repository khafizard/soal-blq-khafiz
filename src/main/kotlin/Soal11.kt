fun main() {

    print("Masukan kata : ")
    val kata = readln().toString().lowercase()

    val bintang = kata.length/2
    var bintangString = ""
    for (i in 0 until bintang){
        bintangString+="*"
    }

    for (i in kata.length-1 downTo 0){
        print(bintangString)
        print(kata[i])
        println(bintangString)
    }

}