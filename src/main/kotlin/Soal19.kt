fun main() {

    print("Masukan Kalimat : ")
    val kalimat = readln().toString().lowercase().replace("[^a-z]".toRegex(), "")

    val alphabet = "abcdefghijklmnopqrstuvwxyz"

    var pangram = true
    for(i in 0 until alphabet.length){
        if(!kalimat.contains(alphabet[i])){
            pangram = false
        }
    }

    if (pangram){
        print("Pangram")
    }else{
        print("Bukan Pangram")
    }

}