fun main() {
    println("Format pemesanan: nama_menu[spasi]harga[spasi]mengandung_ikan(y/n)")
    println("Contoh: Tuna Sandwich 42K y, Spaghetti Carbonara 50K n, Tea Pitcher 30K n, Pizza 70K y")
    print("Masukkan pesanan Anda: ")
    val input = readln().uppercase()
    val pesananNoSpasi = input.replace(" ", "")
    val pesanan = pesananNoSpasi.split(",").toTypedArray()

    var fishMoney = 0
    var noFishMoney = 0
    var number = "0123456789"

    for (i in 0 until pesanan.size) {
        var priceStr = ""
        for (j in 0 until pesanan[i].length) {
            if (number.contains(pesanan[i][j])) {
                priceStr += pesanan[i][j]
            }
        }

        when (pesanan[i].last()) {
            'Y' -> fishMoney += priceStr.toInt() * 1000 / 3
            'N' -> {
                fishMoney += priceStr.toInt() * 1000 / 4
                noFishMoney += priceStr.toInt() * 1000 / 4
            }
        }
    }

    println("Anda membayar: ${(fishMoney * 115 / 100).toInt()}.")
    println("Teman Anda yang alergi ikan membayar: ${(noFishMoney * 115 / 100).toInt()}.")

}