
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

fun main() {
    val localeIndo = Locale("id", "ID")
    val mDtm = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss", localeIndo)

    // Inputan
    print("Tanggal Masuk : ")
    val masuk = readln()
    print("Tanggal Keluar : ")
    val keluar = readln()
    val tglMasuk = LocalDateTime.parse(masuk, mDtm)
    val tglKeluar = LocalDateTime.parse(keluar, mDtm)
    var duration = Duration.between(tglMasuk, tglKeluar)
    var selisihJam = duration.toHours().toInt()
    var tarif1 = hitungTarif(selisihJam)
    println(tarif1)


//    // Inputan Soal
//    var arrMasuk = arrayOf(
//        "27 Januari 2019 05:00:01",
//        "27 Januari 2019 07:03:59",
//        "27 Januari 2019 07:05:00",
//        "27 Januari 2019 11:14:23"
//    )
//    var arrKeluar = arrayOf(
//        "27 Januari 2019 17:45:03",
//        "27 Januari 2019 15:30:06",
//        "28 Januari 2019 00:20:21",
//        "27 Januari 2019 13:20:00"
//    )
//    for (i in 0 until arrMasuk.size) {
//        val tglMasuk = LocalDateTime.parse(arrMasuk[i], mDtm)
//        val tglKeluar = LocalDateTime.parse(arrKeluar[i], mDtm)
//        var duration = Duration.between(tglMasuk, tglKeluar)
//        var selisihJam = duration.toHours().toInt()
//        var tarif = hitungTarif(selisihJam)
//        println("${arrMasuk[i]} -> ${arrKeluar[i]} = $tarif")
//    }
}

fun hitungTarif(jam: Int): Int {
    var tarif = 0
    var sisaJam = jam

    while (sisaJam > 0) {
        if (sisaJam <= 8) {
            tarif = tarif + sisaJam * 1000
            break
        } else if (sisaJam > 8 && sisaJam <= 24) {
            tarif = tarif + 8000
            break
        } else if (sisaJam > 24) {
            sisaJam = sisaJam - 24
            tarif += 15000
        }
    }

    return tarif
}