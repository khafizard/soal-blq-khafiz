import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

fun main() {
    print("Masukan Rentang Tanggal : ")
    val rentangTanggal = readln().toString()

    val splitTanggal = rentangTanggal.split(" – ")


    val formatter = DateTimeFormatter.ofPattern("d MMMM yyyy", Locale("id"))
    val startDate = LocalDate.parse(splitTanggal[0], formatter)
    val endDate = LocalDate.parse(splitTanggal[1], formatter)

    val jumlahHari = ChronoUnit.DAYS.between(startDate, endDate).toInt()

    var totalDenda = 0
    var dendaA = 0
    var dendaB = 0
    var dendaC = 0
    var dendaD = 0

    println("Total Hari : $jumlahHari")

    if (jumlahHari > 14) {
        dendaA = (jumlahHari - 14) * 100
        println("Denda Buku A : $dendaA")
    }
    if (jumlahHari > 3) {
        dendaB = (jumlahHari - 3) * 100
        println("Denda Buku B : $dendaB")
    }
    if (jumlahHari > 7) {
        dendaC = (jumlahHari - 7) * 100
        println("Denda Buku C : $dendaC")
    }
    if (jumlahHari > 7) {
        dendaD = (jumlahHari - 7) * 100
        println("Denda Buku D : $dendaD")
    }

    totalDenda = dendaA + dendaB + dendaC + dendaD
    println("Total Denda : $totalDenda")

}
