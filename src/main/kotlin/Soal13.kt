import kotlin.math.abs

fun main() {
    print("Masukan Jam : ")
    val input = readln().toString().split(":").toTypedArray()

    val jam = input[0].toInt()
    val menit = input[1].toInt()

    val sudut = abs(11.0 / 2 * menit - 30 * jam)

    print(sudut.toInt())


}