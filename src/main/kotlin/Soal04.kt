fun main() {
    print("Masukan n : ")
    val n = readln().toInt()

    var i = 0
    var angka = 1

    do {
        if(isPrime(angka)){
            print("$angka ")
            i++
        }
        angka++
    }while (i < n)
}

fun isPrime(number: Int): Boolean {
    if (number <= 1) {
        return false
    }
    for (i in 2 until number) {
        if (number % i == 0) {
            return false
        }
    }
    return true
}