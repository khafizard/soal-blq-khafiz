
fun main() {

    print("Panjang Deret : ")
    val deret = readln().toInt()
    var arrayFibo = Array(deret) { 0 }
    arrayFibo[0] = 1
    arrayFibo[1] = 1

    for (i in 2..arrayFibo.size - 1) {
        arrayFibo[i] = arrayFibo[i - 2] + arrayFibo[i - 1]
    }

    for (i in arrayFibo) {
        print("$i ")
    }
}