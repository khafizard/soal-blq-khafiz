fun main() {
    print("Catatan Perjalanan : ")
    var catatan = readln()
        .uppercase()
        .split(" ")
        .toTypedArray()

    //N N T N N N T T T T T N T T T N T N
    // N N T N N N T T T T  T  N  T  T  T  N  T  N
    // 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
    var perjalanan = 0
    var gunung = 0
    var lembah = 0

    for (i in 0 until catatan.size) {
        if (catatan[i] == "N") {
            perjalanan += 1
            if (perjalanan == 0) {
                lembah += 1
            }
        } else if (catatan[i] == "T") {
            perjalanan -= 1
            if (perjalanan == 0) {
                gunung += 1
            }
        }
    }
    println("Gunung : $gunung")
    println("Lembah : $lembah")
}