
fun main() {
    print("Masukan Jam : ")
    var waktu = readln()
    var jamSplit = waktu.split(" ").toTypedArray()
    jamSplit = jamSplit[0].split(":").toTypedArray()
    var jam = jamSplit[0].toInt()
    val menit = jamSplit[1]
    val detik = jamSplit[2]

    if (waktu.contains("AM")) {
        if (jam == 0 || jam == 12) {
            print("00:$menit:$detik")
        } else {
            if (jam < 10) {
                print("0$jam:$menit:$detik")
            } else {
                print("$jam:$menit:$detik")
            }
        }
    } else if (waktu.contains("PM")) {
        jam = jam + 12
        if (jam == 24) {
            print("12:$menit:$detik")
        } else {
            print("$jam:$menit:$detik")
        }
    }


}