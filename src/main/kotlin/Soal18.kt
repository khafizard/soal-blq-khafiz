fun main() {
    print("Masukkan list jam saat Donna makan kue (9 13 15 17): ")
    val jam = readln().trim().split(" ").toTypedArray()

    print("Masukkan list kalori kue yang dimakan Donna berturur-turut (30 20 50 80): ")
    val kalori = readln().trim().split(" ").toTypedArray()

    print("Masukkan jam Donna mulai berolahraga (18): ")
    val hourStart = readln().trim().toInt()

    val irisan = minOf(jam.size, kalori.size)
    var time10 = 0.0

    for (i in 0..<irisan) {
        val hour = jam[i].toInt()
        if (hour >= hourStart) {
            break
        } else {
            time10 += 0.1 * kalori[i].toDouble() * (hourStart - hour)
        }
    }

    val cc = (time10 / 30 * 100) + 500
    println("Donna akan minum sebanyak $cc mL")

}